module.exports = function(grunt) {

	require('jit-grunt')(grunt);

	grunt.initConfig({

		pkg: grunt.file.readJSON('package.json'),

		jshint: {
			options: {
				curly: true,
				smarttabs: true
			},
			all: {
				src: 'resources/assets/js/**/*.js'
			}
		},

		clean: {
			dist: ['public/assets']
		},

		watch: {

			gruntfile: {
				files: 'Gruntfile.js',
				tasks: ['default']
			},

			less: {
				files: ['resources/assets/less/**'],
				tasks: ['styles']
			},

			javascript: {
				files: ['resources/assets/js/**'],
				tasks: ['javascript']
			}
		},

		less: {
			site: {
				files: {
					'resources/assets/compiled/css/site.css': 'resources/assets/less/site.less'
				}
			}
		},

		copy: {

			assets: {
				files: [
					{
						expand: true,
						cwd: 'resources/assets',
						src: [
							'img/**',
							'fonts/**'
						],
						dest: 'public/assets/'
					}
				]
			}

    },

		concat: {
			siteCss: {
				src: [
					'resources/assets/compiled/css/site.css'
				],
				dest: 'public/assets/css/site.css',
				nonull: true
			},

			siteJs: {
				src: [
					'bower_components/jquery/dist/jquery.js',

					'bower_components/bootstrap/js/dropdown.js',
					'bower_components/bootstrap/js/collapse.js',
					'bower_components/bootstrap/js/tab.js',

					'resources/assets/js/site.js'
				],
				dest: 'public/assets/js/site.js',
				nonull: true
			},

      adminJs: {
        src: [
          '<%= concat.siteJs.dest %>',
          'resources/assets/js/admin.js',
          'resources/assets/js/pages/admin/*.js'
        ],
        dest: 'public/assets/js/admin.js',
        nonull: true
      }

		},

		cssmin: {
			options: {
				banner: '/*! <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n',
				keepSpecialComments: 0
			},

			siteCss: {
				src: ['<%= concat.siteCss.dest %>'],
				dest: '<%= concat.siteCss.dest %>'
			}
		},

		uglify: {
			options: {
				banner: '/*! <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n',
				compress: true
			},

			siteJs: {
				src: ['<%= concat.siteJs.dest %>'],
				dest: '<%= concat.siteJs.dest %>'
			}
		},

    browserSync: {
      dev: {
        bsFiles: {
          src : [
            'public/assets/css/*.css',
            'public/assets/img/*.**',
            'public/assets/js/*.js'
          ]
        },
        options: {
          watchTask: true
        }
      }
    }

	});

	grunt.registerTask('styles', ['less', 'copy', 'concat:siteCss']);
	grunt.registerTask('javascript', ['concat:siteJs', 'concat:adminJs']);

	grunt.registerTask('default', ['clean', 'styles', 'javascript']);
	grunt.registerTask('dist', function() {
		grunt.task.run(['clean', 'styles', 'javascript', 'cssmin', 'uglify']);
	});
	grunt.registerTask('auto', ['browserSync', 'watch']);

};
